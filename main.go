package main

import (
	"io"
	"log"
	"net/http"
	"os"

	_ "github.com/joho/godotenv/autoload"

	"github.com/gin-gonic/gin"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

// Db is the database connection
var Db *gorm.DB

type DataMahasiswa struct {
	NPM    string `json:"npm" form:"npm" gorm:"primaryKey"`
	Nama   string `json:"nama" form:"nama"`
	Alamat string `json:"alamat" form:"alamat"`
}

type UpdateDataMahasiswa struct {
	Nama   string `json:"nama" form:"nama"`
	Alamat string `json:"alamat" form:"alamat"`
}

func getMahasiswaData(c *gin.Context) {
	idNumber := c.Param("idNumber")

	var dataMahasiswa DataMahasiswa

	if err := Db.Where("npm = ?", idNumber).First(&dataMahasiswa).Error; err != nil {
		c.JSON(400, gin.H{"error": "Record not found"})
		return
	}

	c.JSON(200, dataMahasiswa)
}

func postMahasiswaData(c *gin.Context) {
	var req DataMahasiswa
	if err := c.Bind(&req); err != nil || req.NPM == "" || req.Nama == "" || req.Alamat == "" {
		c.JSON(400, gin.H{"error": err.Error()})
		return
	}

	if dbTrx := Db.Create(&req); dbTrx.Error != nil {
		c.JSON(400, gin.H{"error": dbTrx.Error})
		return
	}

	c.JSON(200, req)
}

func putMahasiswaData(c *gin.Context) {
	idNumber := c.Param("idNumber")
	var newDataMahasiswa UpdateDataMahasiswa
	if err := c.Bind(&newDataMahasiswa); err != nil || newDataMahasiswa.Nama == "" || newDataMahasiswa.Alamat == "" {
		c.JSON(400, gin.H{"error": err.Error()})
		return
	}

	var currentDataMahasiswa DataMahasiswa
	if err := Db.Where("npm = ?", idNumber).First(&currentDataMahasiswa).Error; err != nil {
		c.JSON(400, gin.H{"error": "Record not found"})
		return
	}

	Db.Model(&currentDataMahasiswa).Updates(newDataMahasiswa)

	var updatedDataMahasiswa DataMahasiswa

	if err := Db.Where("npm = ?", idNumber).First(&updatedDataMahasiswa).Error; err != nil {
		c.JSON(400, gin.H{"error": "Record not found"})
		return
	}

	c.JSON(200, updatedDataMahasiswa)
}

func deleteMahasiswaData(c *gin.Context) {
	idNumber := c.Param("idNumber")
	var toBeDeletedDataMahasiswa DataMahasiswa
	if err := Db.Where("npm = ?", idNumber).First(&toBeDeletedDataMahasiswa).Error; err != nil {
		c.JSON(400, gin.H{"error": "Record not found"})
		return
	}

	Db.Delete(&toBeDeletedDataMahasiswa)

	c.JSON(200, gin.H{"npm": idNumber})
}

// Refs: https://developpaper.com/using-go-gin-server-to-do-file-upload-service/
func uploadFile(c *gin.Context) {
	file, header, err := c.Request.FormFile("file")
	if err != nil {
		c.JSON(400, gin.H{"error": err.Error()})
		return
	}

	filename := header.Filename
	saveFile, err := os.Create("public/" + filename)
	if err != nil {
		c.JSON(500, gin.H{"error": err.Error()})
		log.Fatalln(err)
	}
	defer saveFile.Close()

	_, err = io.Copy(saveFile, file)
	if err != nil {
		c.JSON(500, gin.H{"error": err.Error()})
		log.Fatalln(err)
	}

	c.JSON(200, gin.H{"filepath": "file/" + filename})
}

func main() {
	r := gin.Default()

	dsn := os.Getenv("DSN")
	database, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})

	if err != nil {
		panic("Failed to connect database")
	}

	database.AutoMigrate(&DataMahasiswa{})

	Db = database

	mahasiswaAPI := r.Group("/mahasiswa")
	{
		mahasiswaAPI.GET("/:idNumber", getMahasiswaData)
		mahasiswaAPI.POST("", postMahasiswaData)
		mahasiswaAPI.PUT("/:idNumber", putMahasiswaData)
		mahasiswaAPI.DELETE("/:idNumber", deleteMahasiswaData)
	}

	_, err = os.Stat("public")

	if os.IsNotExist(err) {
		errDir := os.MkdirAll("public", 0755)
		if errDir != nil {
			log.Fatal(err)
		}

	}

	fileAPI := r.Group("/file")
	{
		fileAPI.POST("/upload", uploadFile)
		fileAPI.StaticFS("", http.Dir("public"))
	}

	r.Run()
}
